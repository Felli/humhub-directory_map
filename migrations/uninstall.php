<?php
use yii\db\Migration;

class uninstall extends yii\db\Migration
{

    public function up()
    {
        $this->dropTable('directory_map_profile');
    }

    public function down()
    {
        echo "uninstall does not support migration down.\n";
        return false;
    }

}
