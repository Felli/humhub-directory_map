<?php

use yii\db\Migration;

class m180306_104217_initial extends Migration
{
    /*
    public function safeUp()
    {

    }

    public function safeDown()
    {
        echo "m180306_104217_initial cannot be reverted.\n";

        return false;
    }
    */
    
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('directory_map_profile', [
            'id' => 'pk',
            'user_id' => 'int(11) NOT NULL',
            'profile_street' => 'varchar(255) DEFAULT NULL',
            'profile_zip' => 'varchar(255) DEFAULT NULL',
            'profile_city' => 'varchar(255) DEFAULT NULL',
            'profile_country' => 'varchar(255) DEFAULT NULL',
            'profile_state' => 'varchar(255) DEFAULT NULL',
            'lat_zip' => 'float DEFAULT NULL',
            'lng_zip' => 'float DEFAULT NULL',
            'lat_street' => 'float DEFAULT NULL',
            'lng_street' => 'float DEFAULT NULL'
        ], '');
    }

    public function down()
    {
        //echo "m180306_104217_initial cannot be reverted.\n";
        $this->dropTable('directory_map_profile');
        return false;
    }
    
}