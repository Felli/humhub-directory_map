<?php
namespace humhub\modules\directory_map\commands;

use Yii;
use humhub\modules\directory_map\components\Coordinates;


/**
 * @author Marc Farré (marc.fun)
 */


class CronController extends \yii\console\Controller
{

    /**
     * Update coordinates
     */
    public function actionUpdateCoordinates()
    {
        Coordinates::updateAllUsers();

        print "Done !\n\n";
    }
}