<?php

use humhub\widgets\TopMenu;

return [
    'id' => 'directory_map',
    'class' => 'humhub\modules\directory_map\Module',
    'namespace' => 'humhub\modules\directory_map',
    'events' => [
        [
            'class' => TopMenu::class,
            'event' => TopMenu::EVENT_INIT,
            'callback' => [
                'humhub\modules\directory_map\Events',
                'onTopMenuInit'
            ]
        ]
    ]
];
?>