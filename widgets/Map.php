<?php

namespace humhub\modules\directory_map\widgets;

use Yii;
use yii\helpers\Html;
use humhub\components\Widget;
use humhub\models\Setting;
use humhub\modules\user\models\Group;
use humhub\modules\directory_map\models\ProfileField;

class Map extends Widget
{

    public $filter;

    public function init() // normalizes the widget properties
    {
        parent::init();
    }

    public function run() // generates the rendering result
    {
        // Get group list
        $groups = Group::getDirectoryGroups();

        // Get profileFields list
        $profileFieldTable = ProfileField::find();

        // Create HTML layers to add at the end of the <body> tag
        $htmlMapLayers =    '<div id="directory-map-container">';
        $htmlMapLayers .=       '<div id="directory-map-bg"></div>';
        $htmlMapLayers .=       '<div id="directory-map-toolbar">';

        // Add filters
        $filterProfileFields = explode (
            ',',
            str_replace(' ', '', Setting::Get('filterProfileFields', 'directory_map'))
        );

        if (count($filterProfileFields) > 0 AND $filterProfileFields[0] != "") {

            $htmlMapLayers .= '<div id="directory-map-filters">';
            $htmlMapLayers .= '<div id="directory-map-filters-header">'. Yii::t('DirectoryMapModule.base', 'Filter users by:') .'</div>';
            $htmlMapLayers .= '<div id="directory-map-filters-body">';
            $formFieldId = 0;

            foreach ($filterProfileFields as $profileInternalName) {
                $profileField = $profileFieldTable
                    ->where(["internal_name" => $profileInternalName])
                    ->one();
                if ($profileField !== null) {

                    $profileFieldType = substr($profileField->field_type_class, strrpos($profileField->field_type_class, '\\')+1);

                    $htmlMapLayers .= '<div class="filters-profile-field">';
                    $htmlMapLayers .= '<div class="filter-header" data-internal-name="' . $profileInternalName . '" data-field-type="' . $profileFieldType . '">';
                    $htmlMapLayers .= '<input type="checkbox" /><span>' . str_replace("'", "\'", $profileField->title) . '<span>';
                    $htmlMapLayers .= '</div>';
                    $htmlMapLayers .= '<form class="filter-body">';

                    switch ($profileFieldType) {

                        case 'CheckboxList':
                            if (
                                Setting::Get('profileFieldTypeConfigReplacement', 'directory_map') !== null
                                AND Setting::Get('profileFieldTypeConfigReplacement', 'directory_map') != ''
                                AND file_exists(Yii::$app->getBasePath() . '/config/' . Setting::Get('profileFieldTypeConfigReplacement', 'directory_map'))
                            )
                                $replaceList = 
                                    (array) json_decode(
                                        file_get_contents(
                                            Yii::$app->getBasePath() . '/config/' . Setting::Get('profileFieldTypeConfigReplacement', 'directory_map')));
                            $FieldTypeConfig = json_decode($profileField->field_type_config);
                            $FieldOptions = explode ("\r\n", $FieldTypeConfig->options);
                            foreach ($FieldOptions as $option) {
                                $formFieldId++;
                                if (isset ($replaceList))
                                    $optionReadable = $replaceList[$option];
                                else
                                    $optionReadable = $option;
                                $htmlMapLayers .= '<div><input data-option="' . str_replace('"', '\"', str_replace("'", "\'", $option)) . '" type="checkbox" /><span>' . str_replace("'", "\'", $optionReadable) . '</span></div>';
                            }
                            break;
                        
                        // Other classes TBD !

                        default:
                            # code...
                            break;
                    }
                    $htmlMapLayers .=               '</form>';
                    $htmlMapLayers .=           '</div>';

                } // if ($filterProfileFields !== null)
            } // foreach $filterProfileFields

            $htmlMapLayers .= '</div>'; // #directory-map-filters-body
            $htmlMapLayers .= '<div id="directory-map-filters-alert">' . Yii::t('DirectoryMapModule.base', 'No user matching all the filters criterias') . '</div>';
            $htmlMapLayers .= '</div>'; // #directory-map-filters
        }

        // Add close button
        $htmlMapLayers .= '<button id="directory-map-button-close" title="' . Yii::t('DirectoryMapModule.base', 'Close the map') . '">';
        $htmlMapLayers .= '<svg viewBox="0 0 40 40"><path d="M10,10 L30,30 M30,10 L10,30"></path></svg>';
        $htmlMapLayers .= '</button>';
        $htmlMapLayers .= '</div>';

        // Add map
        $htmlMapLayers .= '<div id="directory-map-content">';
        $htmlMapLayers .= '<div id="directory-map-render"></div>';
        $htmlMapLayers .= '</div>';

        // Add legend
        $showGroupLegend = Setting::Get('showGroupLegend', 'directory_map');
        $customLegend = Setting::Get('customLegend', 'directory_map');
        if (
            $showGroupLegend
            || ($customLegend !== null && $customLegend != '')
        ) {
            $htmlMapLayers .= '<div id="directory-map-legend">';
            $htmlMapLayers .= '<div id="directory-map-legend-container">';

            if ($showGroupLegend) {
                $htmlMapLayers .= '<div id="legend-header">' . Yii::t('DirectoryMapModule.base', 'Groups:') . '</div>';
                $htmlMapLayers .= '<div id="legend-body">';
                foreach ($groups as $group) {
                    $htmlMapLayers .= '<span id="legend-group-id-' . $group->id . '">' . str_ireplace("'", "‘", Html::encode($group->name)) . '</span>';
                }
                $htmlMapLayers .=           '</div>';
            }
            // Add custom legend
            if ($customLegend !== null && $customLegend != '') {
                $customLegend = str_ireplace("'", "\'", $customLegend);
                $customLegend = str_ireplace("\n", " ", $customLegend);
                $customLegend = str_ireplace("\r", " ", $customLegend);
                $htmlMapLayers .= $customLegend;
            }
            $htmlMapLayers .=       '</div>';
            $htmlMapLayers .=   '</div>';
        }

        $htmlMapLayers .=   '</div>';
        
        return $this->render('map', [
            'htmlMapLayers' => $htmlMapLayers
        ]);
    }

}
?>