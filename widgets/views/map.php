<?php

use yii\helpers\Html;
use humhub\models\Setting;

$bundle = \humhub\modules\directory_map\Assets::register($this);
$this->registerJsVar('directory_mapJsVar', 42);
?>

<?php if (!Yii::$app->user->isGuest OR Setting::Get('auth.allowGuestAccess', 'user')) : // if user is registred or the non registered users are allowed to a limited access ?>
	<script type="text/javascript">
		var baseUrl = '<?= Yii::$app->getUrlManager()->getBaseUrl(); ?>';
		var htmlMapLayers = '<?= $htmlMapLayers; ?>';
		$('a[href$="/directory_map/view"]').attr('href', '#').click(function(){
	        CreateOrShowMap ();
	    });
	</script>
<?php endif; ?>