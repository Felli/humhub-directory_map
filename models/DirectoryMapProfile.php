<?php

namespace humhub\modules\directory_map\models;

use Yii;
use yii\db\ActiveRecord;
use humhub\modules\user\models\User;

class DirectoryMapProfile extends ActiveRecord {

    /**
     * @return string the associated database table name
     */
    public static function tableName()
    {
        return 'directory_map_profile';
    }

    public function getUsers()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

	function userCoordinatesFromUserId ($userId) {
		
	}

	function usersWithoutCoordinates () {
		
	}

}

?>