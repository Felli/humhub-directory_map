<?php

namespace humhub\modules\directory_map\models;

use Yii;

/**
 * ConfigureForm defines the configurable fields.
 *
 * @author Marc Farré (marc.fun)
 */
class ConfigureForm extends \yii\base\Model
{

    public $googleGeocodingKey;
    public $popupProfileFields;
    public $filterProfileFields;
    public $showGroupLegend;
    public $customLegend;
    public $profileFieldTypeConfigReplacement;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return [
            [['googleGeocodingKey'], 'required'],
            ['popupProfileFields', 'string'],
            ['filterProfileFields', 'string'],
            ['showGroupLegend', 'boolean'],
            ['customLegend', 'string'],
            ['profileFieldTypeConfigReplacement', 'string']
        ];
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return [
            'googleGeocodingKey' => Yii::t('DirectoryMapModule.config', 'Google geocoding API key'),
            'popupProfileFields' => Yii::t('DirectoryMapModule.config', 'List of profile fiels to display in the marker popup (write the list of internal names seperated by commas)'),
            'filterProfileFields' => Yii::t('DirectoryMapModule.config', 'List of profile fiels to display in the users filter form (write the list of internal names seperated by commas)'),
            'showGroupLegend' => Yii::t('DirectoryMapModule.config', 'Show a legend of all visible groups'),
            'customLegend' => Yii::t('DirectoryMapModule.config', 'HTML code for a customized legend'),
            'profileFieldTypeConfigReplacement' => Yii::t('DirectoryMapModule.config', 'Json file in path-to-humhub/protected/config to replace the field_type_config values of a profile field if CheckboxList (optional)')
        ];
    }
}
