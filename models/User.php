<?php

namespace humhub\modules\directory_map\models;

use Yii;
use yii\db\ActiveRecord;

class Profile extends ActiveRecord {}
class Group extends ActiveRecord {}
class GroupUser extends ActiveRecord {}
class DirectoryMapProfile extends ActiveRecord {}

class User extends ActiveRecord {

    public function getProfile()
    {
        return $this->hasOne(Profile::class, ['user_id' => 'id']);
    }

    public function getGroupUsers()
    {
        return $this->hasMany(GroupUser::class, ['user_id' => 'id']);
    }

    public function getGroups()
    {
        return $this->hasMany(Group::class, ['id' => 'group_id'])->via('groupUsers');
    }

    public function getDirectoryMapProfile()
    {
        return $this->hasMany(DirectoryMapProfile::class, ['user_id' => 'id']);
    }
}