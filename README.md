Content Module Directory Map 
=================

## Description

Curently in developpment

## TBD

- Make other filters types work (currently only profile CheckboxList are available).
- Show module in a space

## Repository

https://gitlab.com/funkycram/humhub-modules-directory_map

## Setup Instructions:

`cd protected/modules; git clone git@gitlab.com:funkycram/humhub-directory_map.git directory_map;`

Ater module activation, go to configuration (directory_map/config) and read the instructions

To customize a specific markers of specific groups with css less use the above example in your theme.
Replace @directoryMapMarkerList by the id of the groups and the colors you want.
If a user blongs to multiple groups, the group show the one having the higher sort_order number.

```
// Function (do not change)
.setDirectoryMapMarker (@groupId, @color, @bgColor) {
	#directory-map-container {
		path.leaflet-marker-group-id-@{groupId} {
		    fill: @bgColor;
		}
		#directory-map-legend #directory-map-legend-container #legend-body span {
			&#legend-group-id-@{groupId} {
				background-color: @bgColor;
				color: @color;
			}
		}
	}
}

// Your colors
@color1: #800;
@bdColor2: #FCC;
@color1: #080;
@bdColor2: #CFC;

// Marker colors list (1 and 2 are group ids)
@directoryMapMarkerList:
	1 @color1 @bdColor2,
	2 @color1 @bdColor2;

// Loop to exute the function (do not change)
.directoryMapMarkerLoop(@index) when (@index > 0){
    @directoryMapMarkerArray: extract(@directoryMapMarkerList, @index); //extract based on comma separator
    @groupId: extract(@directoryMapMarkerArray,1);
    @color: extract(@directoryMapMarkerArray,2);
    @bgColor: extract(@directoryMapMarkerArray,3);
	.setDirectoryMapMarker (@groupId, @color, @bgColor);
    .directoryMapMarkerLoop(@index - 1);
}
.directoryMapMarkerLoop(length(@directoryMapMarkerList)); //pass the length as counter (split based on comma)
```

## Author:

Marc Farré (https://marc.fun)