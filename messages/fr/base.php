<?php
return array (
	'Directory map' => 'Cartographie des membres',
	'Map' => 'Carte',
	'Show the users map' => 'Afficher la carte des membres',
	'Show on a map' => 'Afficher sur une carte',
	'Close the map' => 'Fermer la carte',
	'Filter users by:' => 'Filtrer les membres par :',
	'No user matching all the filters criterias' => 'Aucun membre correspond à tous les criètres de ce filtre',
	'Groups:' => 'Rôles :',
);
?>