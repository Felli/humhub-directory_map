<?php

use humhub\models\Setting;
use yii\helpers\Url;
use yii\helpers\Html;
use humhub\compat\CActiveForm;
?>

<div class="panel panel-default">

    <div class="panel-heading"><?= Yii::t('DirectoryMapModule.config', '<strong>Directory map</strong> module configuration'); ?></div>

    <div class="panel-body">
        
        <?php $form = CActiveForm::begin(); ?>

        <?= $form->errorSummary($model); ?>

        <div class="form-group">
            <?= $form->labelEx($model, 'googleGeocodingKey'); ?>
            <?= $form->textField($model, 'googleGeocodingKey', ['class' => 'form-control']); ?>
            <?= $form->error($model, 'googleGeocodingKey'); ?>
            <p class="help-block"><a href="https://developers.google.com/maps/documentation/geocoding/get-api-key?hl=fr" target="_blank"><?= Yii::t('DirectoryMapModule.config', 'Get your Google Geocoding API Key here'); ?></a></p>
        </div>

        <div class="form-group">
            <?= $form->labelEx($model, 'popupProfileFields'); ?>
            <?= $form->textField($model, 'popupProfileFields', ['class' => 'form-control']); ?>
            <?= $form->error($model, 'popupProfileFields'); ?>
            <p class="help-block"><?= Yii::t('DirectoryMapModule.config', 'e.g. : mobile,job'); ?></p>
        </div>

        <div class="form-group">
            <?= $form->labelEx($model, 'filterProfileFields'); ?>
            <?= $form->textField($model, 'filterProfileFields', ['class' => 'form-control']); ?>
            <?= $form->error($model, 'filterProfileFields'); ?>
            <p class="help-block"><?= Yii::t('DirectoryMapModule.config', 'e.g. : mobile,job'); ?></p>
        </div>

        <div class="form-group">
            <?= $form->labelEx($model, 'showGroupLegend'); ?>
            <?= $form->checkBox($model, 'showGroupLegend'); ?>
            <?= $form->error($model, 'showGroupLegend'); ?>
        </div>

        <div class="form-group">
            <?= $form->labelEx($model, 'customLegend'); ?>
            <?= $form->textArea($model, 'customLegend', ['class' => 'form-control']); ?>
            <?= $form->error($model, 'customLegend'); ?>
        </div>

        <div class="form-group">
            <?= $form->labelEx($model, 'profileFieldTypeConfigReplacement'); ?>
            <?= $form->textField($model, 'profileFieldTypeConfigReplacement', ['class' => 'form-control']); ?>
            <?= $form->error($model, 'profileFieldTypeConfigReplacement'); ?>
            <p class="help-block"><?= Yii::t('DirectoryMapModule.config', 'e.g. : my-folder/profile_replace_list.json'); ?></p>
        </div>

        <hr>

        <h4><?= Yii::t('DirectoryMapModule.config', 'Please make sure following cronjobs are installed:'); ?></h4>
        

        <p><?= Yii::t('DirectoryMapModule.config', 'edit protected/config/console.php and add (as a new element of the return array):'); ?></p>
        <pre>
          'bootstrap' => [
              'directory_map',
          ],
          'modules' => [
              'directory_map' => [
                  'class' => 'humhub\modules\directory_map\Module',
              ],
          ],
        </pre>

        <p><?= Yii::t('DirectoryMapModule.config', 'Crontab to add (any user):'); ?></p>
        <pre>55 * * * * php <?= Yii::$app->getBasePath() ?>/yii directory_map/cron/update-coordinates</pre>

        <hr>

        <h4><?= Yii::t('DirectoryMapModule.config', 'Make the menu button show directly the map in a lightbox:'); ?></h4>

        <p><?= Yii::t('DirectoryMapModule.config', 'Add this code to your theme file "views/layouts/main.php", just after "&lt;!-- end: second top navigation bar --&gt;":'); ?></p>
        <pre>&lt;?= \humhub\modules\directory_map\widgets\Map::widget(['filter' =&gt; '']); ?&gt;</pre>

        <p><?= Yii::t('DirectoryMapModule.config', 'To customize markers with CSS in your theme, see README.md'); ?></p>

        <hr>

        <?= Html::submitButton(Yii::t('DirectoryMapModule.config', 'Save'), ['class' => 'btn btn-primary']); ?>

        <a class="btn btn-default" href="<?= Url::to(['/admin/module']); ?>"><?= Yii::t('DirectoryMapModule.config', 'Back to modules'); ?></a>

        <?php CActiveForm::end(); ?>

        <h4><?= Yii::t('DirectoryMapModule.config', 'AFTER SAVING THIS FORM:'); ?></h4>
        
        <p><?= Yii::t('DirectoryMapModule.config', 'For the first time, just after saving, create the coordinates in the database by'); ?> <a href="<?= yii\helpers\Url::base(); ?>/directory_map/map/update-coordinates"><?= Yii::t('DirectoryMapModule.config', 'browsing this page'); ?></a> <?= Yii::t('DirectoryMapModule.config', '(might be very long to load, please wait and if needed reload it several times until you see the text)'); ?></p>       

    </div>
</div>
