<?php

use yii\helpers\Url;
use yii\helpers\Html;
use humhub\models\Setting;

$bundle = \humhub\modules\directory_map\Assets::register($this);
$this->registerJsVar('directory_mapJsVar', 42);
?>

<?php if (!Yii::$app->user->isGuest OR Setting::Get('auth.allowGuestAccess', 'user')) : // if user is registred or the non registered users are allowed to a limited access ?>
    <div class="content-directory_map-module-container">
        <div class="panel panel-default">
        
            <div class="panel-heading">
                <h1>
                    <strong></strong>
                </h1>
            </div>
        
            <div class="panel-body">
                <?= \humhub\modules\directory_map\widgets\Map::widget(['filter' => '']); ?>
                <a id="directory-map-open" class="directory-map tt" href="#" onclick="CreateOrShowMap ();" data-original-title="<?= str_replace('"', '\"', Yii::t('DirectoryMapModule.base', 'Show the users map')); ?>" data-placement="bottom">
                    <i class="fa fa-map"></i> <?= str_replace('"', '\"', Yii::t('DirectoryMapModule.base', 'Show the users map')); ?></i>
                </a>
            </div>
        
        </div>
    </div>
<?php endif; ?>