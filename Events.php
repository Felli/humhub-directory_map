<?php

namespace humhub\modules\directory_map;

use humhub\models\Setting;
use Yii;

/**
 * DirectoryMap module event handling.
 *
 * @author Marc Farré (marc.fun)
 */

class Events extends \yii\base\BaseObject
{
 	/**
     * On build of the TopMenu, check if module is enabled
     * When enabled add a menu item
     *
     * @param type $event
     */
    public static function onTopMenuInit($event)
    {
        if (!Yii::$app->user->isGuest OR Setting::Get('auth.allowGuestAccess', 'user')) { // if user is registred or the non registered users are allowed to a limited access
            $event->sender->addItem([
                'label' => Yii::t('DirectoryMapModule.base', 'Directory map'),
                'id' => 'directory_map',
                'icon' => '<i class="fa fa-map"></i>',
                'url' => \yii\helpers\Url::to(['/directory_map/view']),
                'sortOrder' => 400,
                'isActive' => (Yii::$app->controller->module && Yii::$app->controller->module->id == 'directory_map'),
            ]);
        }
    }
}
