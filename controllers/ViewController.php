<?php

namespace humhub\modules\directory_map\controllers;

use Yii;

/**
 * @author Marc Farré (marc.fun)
 */
class ViewController extends \humhub\components\Controller
{

    // the sublayout that should be used
    public $subLayout = "@humhub/modules/directory_map/views/_layout";

    public function init()
    {
        // the pagetitle that will show up
        $this->appendPageTitle(\Yii::t('DirectoryMapModule.base', 'Directory map'));
        return parent::init();
    }

    public function actionIndex()
    {
        return $this->render('index', []);
    }
}
