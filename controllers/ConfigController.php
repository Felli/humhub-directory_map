<?php
namespace humhub\modules\directory_map\controllers;

use Yii;
use humhub\modules\directory_map\models\ConfigureForm;
use humhub\models\Setting;

/**
 * ConfigController handles the configuration requests.
 *
 * @author Marc Farré (marc.fun)
 */

class ConfigController extends \humhub\modules\admin\components\Controller
{

    /**
     * Configuration action for super admins.
     */
    public function actionIndex()
    {
        $form = new ConfigureForm();
        $form->googleGeocodingKey = Setting::Get('googleGeocodingKey', 'directory_map');
        $form->popupProfileFields = Setting::Get('popupProfileFields', 'directory_map');
        $form->filterProfileFields = Setting::Get('filterProfileFields', 'directory_map');
        $form->showGroupLegend = Setting::Get('showGroupLegend', 'directory_map');
        $form->customLegend = Setting::Get('customLegend', 'directory_map');
        $form->profileFieldTypeConfigReplacement = Setting::Get('profileFieldTypeConfigReplacement', 'directory_map');

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $form->googleGeocodingKey = Setting::Set('googleGeocodingKey', $form->googleGeocodingKey, 'directory_map');
            $form->popupProfileFields = Setting::Set('popupProfileFields', $form->popupProfileFields, 'directory_map');
            $form->filterProfileFields = Setting::Set('filterProfileFields', $form->filterProfileFields, 'directory_map');
            $form->showGroupLegend = Setting::Set('showGroupLegend', $form->showGroupLegend, 'directory_map');
            $form->customLegend = Setting::Set('customLegend', $form->customLegend, 'directory_map');
            $form->profileFieldTypeConfigReplacement = Setting::Set('profileFieldTypeConfigReplacement', $form->profileFieldTypeConfigReplacement, 'directory_map');
            return $this->redirect(['/directory_map/config']);
        }

        return $this->render('index', [
            'model' => $form
        ]);
    }
}

?>
