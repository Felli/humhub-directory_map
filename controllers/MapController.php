<?php

namespace humhub\modules\directory_map\controllers;

use Yii;
use yii\helpers\Html;
use yii\helpers\BaseStringHelper;
use yii\helpers\Url;
use humhub\modules\directory_map\models\User;
use humhub\modules\directory_map\models\ProfileField;
use humhub\modules\directory_map\models\DirectoryMapProfile;
use humhub\modules\directory_map\components\Coordinates;
use humhub\modules\admin\widgets\AdminMenu;
use humhub\models\Setting;

/**
 * @author Marc Farré (marc.fun)
 */
class MapController extends \humhub\components\Controller

{
 /* ======================================================== */
 /* CALLED BY AJAX - RETURN LIST OF USERS TO SHOW ON THE MAP */
 /* ======================================================== */
 public function actionGeojsondata()
 {
  if (!Yii::$app->user->isGuest OR Setting::Get('auth.allowGuestAccess', 'user'))
  { // if user is registred or the non registered users are allowed to a limited access

   // Get users

   $usersTable = User::find()->joinWith('profile')->joinWith('directoryMapProfile');

   // Add filters

   if (isset($_POST['filters']) AND is_array($_POST['filters']))
   {
    $filter = $_POST['filters'];

    // Filter profile fields

    foreach($filter["profile"] as $profileInternalName => $filterField)
    {
     if (is_array($filterField))
     {
      switch ($filterField["fieldType"])
      {
      case 'CheckboxList':
       if (isset($filterField["optionsChecked"]))
       {
        $optionsChecked = $filterField["optionsChecked"];
        $usersTable->andWhere(["LIKE", "profile." . $profileInternalName, $optionsChecked]);
       }

       break;

       // Others field types TBD

      default:

       // code...

       break;
      }
     }
    }
   }

   $users = $usersTable->all();

   // Get users coordinates

   $coordinatesTable = DirectoryMapProfile::find()->all();
   foreach($coordinatesTable as $coordinate)
   {
    $coordinates[$coordinate->user_id] = ['lat_zip' => $coordinate['lat_zip'], 'lng_zip' => $coordinate['lng_zip'], 'lat_street' => $coordinate['lat_street'], 'lng_street' => $coordinate['lng_street']];
   }

   // Create geojsonData

   $geojsonData["type"] = "FeatureCollection";
   $geojsonData["features"] = [];
   $userIsAdmin = AdminMenu::canAccess();
   foreach($users as $user):
    if (isset($coordinates[$user->id])):

     // Choose if coordinates precision is zip (admin) or street

     $memberCoordinate = []; // https://tools.ietf.org/html/rfc7946#section-4
     if ($coordinates[$user->id]['lng_street'] !== null AND $userIsAdmin)
     {
      $memberCoordinate = [(float)$coordinates[$user->id]['lng_street'], (float)$coordinates[$user->id]['lat_street']];
     }
     elseif ($coordinates[$user->id]['lng_zip'] !== null)
     {
      $memberCoordinate = [(float)$coordinates[$user->id]['lng_zip'], (float)$coordinates[$user->id]['lat_zip']];
     }

     // If member has coordinates

     if (count($memberCoordinate) == 2):

      // Get member groups

      $memberGroups = $user->groups;
      $memberGroupsHtml = '';
      $memberGroupsId = [];
      $memberGroupsSortOrder = [];
      foreach($memberGroups as $key => $memberGroup)
      {
       if ($memberGroup->show_at_directory)
       {
        $memberGroupsHtml.= '<span>' . Html::encode($memberGroup->name) . '</span>';
        $memberGroupsId[$key] = $memberGroup->id;
        $memberGroupsSortOrder[$key] = $memberGroup->sort_order;
       }
       else unset($memberGroups[$key]);
      }

      if (isset($memberGroups[0])):

       // Get main group (smaller sort_order number)

       array_multisort($memberGroupsSortOrder, SORT_NUMERIC, SORT_DESC, $memberGroupsId, $memberGroups);
       $memberMainGroupId = $memberGroups[0]['id'];

       // Create profile (marker popup)

       $JsonProfile = '<div class="u-n"><a onclick="ount(\'' . $user->username . '\');">' . Html::encode(ucfirst($user->profile->firstname)) . ' ' . Html::encode(ucfirst($user->profile->lastname)) . '</a></div><div class="u-g">' . $memberGroupsHtml . '</div>';

       // Add some specific profile fields

       $popupProfileFields = explode(',', str_replace(' ', '', Setting::Get('popupProfileFields', 'directory_map')));
       if (count($popupProfileFields) > 0)
       {
        $profileFieldTable = ProfileField::find();
        $JsonProfile.= '<div class="u-p">';
        foreach($popupProfileFields as $profileInternalName)
        {
         if (isset($user->profile->$profileInternalName) AND $user->profile->$profileInternalName !== null)
         {
          $profileField = $profileFieldTable->where(["internal_name" => $profileInternalName])->one();
          $JsonProfile.= '<div class="u-p-' . $profileField->id . '">' . Html::encode(BaseStringHelper::truncate($profileField->title, 50)) . ' : ' . $user->profile->$profileInternalName . '</div>';
         }
        }

        $JsonProfile.= '</div>';
       }

       $feature = ["type" => "Feature", "properties" => ["profile" => $JsonProfile, "mainGroupId" => $memberMainGroupId], "geometry" => ["type" => "Point", "coordinates" => $memberCoordinate]];
       array_push($geojsonData["features"], $feature);
      endif;
     endif;
    endif;
   endforeach;
   header('Content-type: application/json');
   echo json_encode($geojsonData);
  }
 }

 public function actionUpdateCoordinates()
 {
  Coordinates::updateAllUsers();
 }
}

?>