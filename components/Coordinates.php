<?php

namespace humhub\modules\directory_map\components;

use Yii;
use humhub\modules\directory_map\models\User;
use humhub\modules\directory_map\models\DirectoryMapProfile;
use humhub\models\Setting;


/**
 * @author Marc Farré (marc.fun)
 */

class Coordinates extends \yii\base\Component
{
    /* ====================================================== */
    /* CALLED BY CRON - UPDATE USERS COORDINATES IN THE TABLE */
    /* ====================================================== */

    public static function updateAllUsers() {
        $users = User::find()
            ->joinWith('groups')
            ->andWhere(['group.show_at_directory' => true])
        	->all();

		$directoryMapProfileTable = DirectoryMapProfile::find();

        $numberOfCoordinatesAlreadyUpToDate = 0;
        $numberOfCoordinatesStillUnknown = 0;
        $numberOfCoordinatesUpdated = 0;
        $numberOfZipCoordinatesNotUpdatable = 0;
        $numberOfStreetCoordinatesNotUpdatable = 0;
        $mailListingToAskToUpdateAddress = '';

        ?><h2>Results :</h2><?php

        foreach ($users as $user) {

            $memberMapUser = $directoryMapProfileTable
            	->where (["user_id" => $user->id])
            	->one();
            if (
            	$memberMapUser != null
            	AND $memberMapUser->profile_street == $user->profile->street
            	AND $memberMapUser->profile_zip == $user->profile->zip
            	AND $memberMapUser->profile_city == $user->profile->city
            	AND $memberMapUser->profile_state == $user->profile->state
            	AND $memberMapUser->profile_country == $user->profile->country
            ) {
            	// Do nothing, cordinates already up to date
            	$numberOfCoordinatesAlreadyUpToDate++;

            	if ($memberMapUser->lat_zip == null)
            		echo 'Zip coordinates of ' . $user->profile->firstname . '' . $user->profile->lastname . ' are still unknown<br />\r';
            	if ($memberMapUser->lat_street == null)
            		echo 'Street coordinates of ' . $user->profile->firstname . '' . $user->profile->lastname . ' are still unknown<br />\r';
            	if ($memberMapUser->lat_zip == null OR $memberMapUser->lat_street == null) {
            		$numberOfCoordinatesStillUnknown++;
            		$mailListingToAskToUpdateAddress .= ','.$user->email;
            	}
            }

            else {

	            if ($memberMapUser == null) {
	            	$memberMapUser = new DirectoryMapProfile;
	            	$memberMapUser->user_id = $user->id;
	            }

        		$memberMapUser->profile_street = $user->profile->street;
        		$memberMapUser->profile_zip = $user->profile->zip;
        		$memberMapUser->profile_city = $user->profile->city;
        		$memberMapUser->profile_state = $user->profile->state;
        		$memberMapUser->profile_country = $user->profile->country;

                // Check if user has selected a country in his profile
                if ($user->profile->country === null OR $user->profile->country == '') {
                    echo $user->profile->firstname . '' . $user->profile->lastname . ' has no Country selected in his profile<br />\r';
                    $mailListingToAskToUpdateAddress .= ','.$user->email;
                    $numberOfZipCoordinatesNotUpdatable++;
                    $numberOfStreetCoordinatesNotUpdatable++;
                }
                
                else {
                    // Get Coordinates at a zip scale
    	            $coordinatesZip = static::getFromAPI (
    	            	'',
    	                $user->profile->zip,
    	                $user->profile->city,
    	                $user->profile->state,
    	                $user->profile->country
    	            );
    	            if ($coordinatesZip == "NOT_FOUND") {
    	            	echo 'Could not get zip coordinates of ' . $user->profile->firstname . '' . $user->profile->lastname . '<br />\r';
    	            	$numberOfZipCoordinatesNotUpdatable++;
    	            }
    	            else {
    		        	$memberMapUser->lat_zip = $coordinatesZip['lat'];
    		        	$memberMapUser->lng_zip = $coordinatesZip['lng'];
    		        }

    		        // Get Coordinates at a street scale
    	            $coordinatesStreet = static::getFromAPI (
    	            	$user->profile->street,
    	                $user->profile->zip,
    	                $user->profile->city,
    	                $user->profile->state,
    	                $user->profile->country
    	            );
    	            if ($coordinatesStreet == "NOT_FOUND") {
    	            	echo 'Could not get street coordinates of ' . $user->profile->firstname . '' . $user->profile->lastname . '<br />\r';
    	            	$numberOfStreetCoordinatesNotUpdatable++;
    	            }
    	            else {
    		        	$memberMapUser->lat_street = $coordinatesStreet['lat'];
    		        	$memberMapUser->lng_street = $coordinatesStreet['lng'];
    				}

    	            $memberMapUser->save();

    	            if ($coordinatesStreet != 'NOT_FOUND' OR $coordinatesZip != 'NOT_FOUND') {
    	            	$numberOfCoordinatesUpdated++;
    		        }
    		        if ($coordinatesStreet == 'NOT_FOUND' OR $coordinatesZip == 'NOT_FOUND') {
    		       		$mailListingToAskToUpdateAddress .= ','.$user->email;
    		       	}
                }
            }
        }

        // Remove users deleted or in a group not shown in the directory
        $directoryMapProfileTable = DirectoryMapProfile::find()->all();
        foreach ($directoryMapProfileTable as $profile) {
            $deleteProfile = true;
            foreach ($users as $user) {
                if ($user['id'] == $profile['user_id'])
                    $deleteProfile = false;
            }
            if ($deleteProfile)
                $profile->delete();
        }
        ?>

        <br /><hr /><br /><h2>Statistics :</h2>
        <?= $numberOfCoordinatesUpdated . ' coordinates updated<br />\r'
        	. $numberOfZipCoordinatesNotUpdatable . ' coordinates at a zip scale couldn‘t be updated<br />\r'
        	. $numberOfStreetCoordinatesNotUpdatable . ' coordinates at a street scale couldn‘t be updated<br />\r'
        	. '<br />\r'
        	. $numberOfCoordinatesStillUnknown . " users has coordinates still unknown<br />\r"
        	. $numberOfCoordinatesAlreadyUpToDate . " users are already up to date<br />\r"; ?>
        <br /><hr /><br /><h2>Ask theses users to update their address :</h2>
        <textarea style="width: 100%; min-height: 200px;"><?= substr($mailListingToAskToUpdateAddress, 1); ?></textarea>

        <?php
	}

	/* =================================================== */

    protected static function getFromAPI ($street, $zip, $city, $state, $country) {
    	$address = urlencode($street);
    	$components = [];
    	if ($zip != '')
    		$components[] = 'postal_code:'.urlencode($zip);
    	if ($city != '')
    		$components[] = 'locality:'.urlencode($city);
    	if ($state != '')
    		$components[] = 'administrative_area:'.urlencode($state);
    	if ($country != '')
    		$components[] = 'country:'.urlencode($country);
    	$components = implode ('|', $components);

        $response = file_get_contents(
        	"https://maps.googleapis.com/maps/api/geocode/json?".
        	"address=".$address.
        	"&components=".$components.
        	"&key=" . str_replace(' ', '', Setting::Get('googleGeocodingKey', 'directory_map'))
        );
        $coordinates = json_decode($response,TRUE); //generate array object from the response from the web

        if (
            isset ($coordinates['results'])
            AND isset ($coordinates['results'][0])
            AND isset ($coordinates['results'][0]['geometry'])
            AND isset ($coordinates['results'][0]['geometry']['location'])
        )
            return $coordinates['results'][0]['geometry']['location'];
        else
            return 'NOT_FOUND';
    }
}