// Define global variables
var markers;
var map;
var geoJsonLayer;

function CreateOrShowMap () {
	if (!$("#directory-map-container").length) { // Prevent to load it twice
		$("body").append(htmlMapLayers);

		$("#directory-map-button-close, #directory-map-bg").click(function(){
			hm ("");
		});

		var tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
			maxZoom: 18,
			attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
		});
		map = L.map('directory-map-render')
			.addLayer(tiles);
		L.control.scale().addTo(map);
		markers = L.markerClusterGroup();

		// Add filter listener
		var filters = {
			'space': {},
			'groups': {},
			'profile': {}
		};
		var options = [];
		var internalName;

		addDataToMapFromAjax (filters);

		$("#directory-map-filters-body .filters-profile-field .filter-header").click(function(){
			internalName = $(this).data("internal-name");

			if ($(this).parent(".filters-profile-field.active").length) { // if we want to desactivate
		    	$(this).parent(".filters-profile-field").removeClass("active");
	    		$(this).find(":checkbox").prop('checked', false);
		    	delete filters.profile[internalName];
		    }

		    else { // if we want to activate
				$(this).parent(".filters-profile-field").addClass("active");
		    	$(this).find(":checkbox").prop('checked', true);

				optionsChecked = [];
				$(this).parent(".filters-profile-field").find("form.filter-body input:checked").each(function() {
					optionsChecked.push($(this).data("option"));
				});

				filters.profile[internalName] = {
					'fieldType': 'CheckboxList',
					'optionsChecked': optionsChecked
				};
		    }
	    	filterMap (filters);
		});


		$("#directory-map-filters-body form.filter-body >div").click(function() {
			optionsChecked = [];
			if ($(this).hasClass("active")) {
				$(this).removeClass("active");
				$(this).find(":checkbox").prop('checked', false);
			}
			else {
				$(this).addClass("active");
				$(this).find(":checkbox").prop('checked', true);
			}

			$(this).parent("form.filter-body").find("input:checked").each(function() {
				optionsChecked.push($(this).data("option"));
			});
			internalName = $(this).parents(".filters-profile-field").find(".filter-header").data("internal-name");
			filters.profile[internalName] = {
				'fieldType': 'CheckboxList',
				'optionsChecked': optionsChecked
			};
	    	filterMap (filters);
		});
	}
}

// Hide Map
function hm (userName) {
	map.removeLayer(markers);
	markers.removeLayer(geoJsonLayer);
	$("#directory-map-container").remove();
	if (userName != '')
		humhub.modules.client.pjax.redirect(baseUrl + '/u/' + userName + '/user/profile/about');
}

// Open User on a New Tab
function ount (userName) {
	if (userName != '')
		window.open(baseUrl + '/u/' + userName + '/user/profile/about', '_blank');
}

function filterMap (filters) {
	map.removeLayer(markers);
	markers.removeLayer(geoJsonLayer);
	addDataToMapFromAjax (filters);
}

function addDataToMapFromAjax (filters) {
	$.ajax({
        type: "POST",
        url: baseUrl + "/directory_map/map/geojsondata",
        dataType: "json",
        data: {
        	filters: filters // show only users matching the filters
        },
        success: function (geojsonData)
        {
	        geoJsonLayer = L.geoJson(
				geojsonData,
				{
					onEachFeature: function (feature, layer) {
						layer.bindPopup(feature.properties.profile);
					},
		            pointToLayer: function(feature, latlng) {
		                return L.circleMarker(latlng, {
							radius: 15,
							fillColor: "#00a9ce",
							color: "#000",
							weight: 0,
							opacity: 1,
							fillOpacity: 0.8,
							className: 'leaflet-marker-group-id-' + feature.properties.mainGroupId
						});
		            }
				}
			);
			markers.addLayer(geoJsonLayer);
			map.addLayer(markers);
			map.fitBounds(markers.getBounds());

        	if (geojsonData.features.length > 0)
        		$("#directory-map-filters-alert").hide();
        	else
        		$("#directory-map-filters-alert").show();
        }
    });
}