<?php

namespace humhub\modules\directory_map;

use yii\web\AssetBundle;

class Assets extends AssetBundle
{

    public $publishOptions = [
        'forceCopy' => true
    ];

    public $css = [
        'css/module.css'
    ];

    public $js = [
        'js/leaflet.js',
        'js/leaflet.markercluster.js',
        'js/module.min.js?v=1.0'
    ];

    public function init()
    {
        $this->sourcePath = dirname(__FILE__) . '/assets';
        parent::init();
    }
}
